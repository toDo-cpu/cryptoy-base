import hashlib
import os
from random import (
    Random,
)

import names


def hash_password(password: str) -> str:
    return hashlib.sha3_256(password.encode()).hexdigest()


def random_salt() -> str:
    return bytes.hex(os.urandom(32))


def generate_users_and_password_hashes(
    passwords: list[str], count: int = 32
) -> dict[str, str]:
    rng = Random()  # noqa: S311

    users_and_password_hashes = {
        names.get_full_name(): hash_password(rng.choice(passwords))
        for _i in range(count)
    }
    return users_and_password_hashes


def attack(passwords: list[str], passwords_database: dict[str, str]) -> dict[str, str]:
    users_and_passwords = {}

    # A implémenter
    # Doit calculer le mots de passe de chaque utilisateur grace à une attaque par /turingtoy-basedictionnaire 

    hashed_passwords = { hash_password(password): password for password in passwords }

    for user, hashed_password in passwords_database.items():
        if hashed_password in hashed_passwords.keys():
            users_and_passwords[user] = hashed_passwords[hashed_password]
    return users_and_passwords


def fix(
    passwords: list[str], passwords_database: dict[str, str]
) -> dict[str, dict[str, str]]:
    users_and_passwords = attack(passwords, passwords_database)

    # A implémenter
    # Doit calculer une nouvelle base de donnée ou chaque élement est un dictionnaire de la forme:
    # {
    #     "password_hash": H,
    #     "password_salt": S,
    # }
    # tel que H = hash_password(S + password)
    new_database = {}
    for user, password in users_and_passwords.items():
        S = random_salt()
        H = hash_password(S + password)
        new_database[user] = {
            "password_hash": H,
            "password_salt": S,
        }
    return new_database


def authenticate(
    user: str, password: str, new_database: dict[str, dict[str, str]]
) -> bool:
    #Doit renvoyer True si l'utilisateur a envoyé le bon password, False sinon
    if user not in new_database.keys():
        return False

    user_hashed_password = new_database[user]["password_hash"]
    user_salt = new_database[user]["password_salt"]
    hashed_password = hash_password(user_salt + password )
    print(hashed_password, user_hashed_password)
    return user_hashed_password == hashed_password
